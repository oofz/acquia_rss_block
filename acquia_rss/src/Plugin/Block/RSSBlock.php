<?php

namespace Drupal\acquia_rss\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "acquia_rss_block",
 *   admin_label = @Translation("Acquia RSS block"),
 * )
 */
class RSSBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->getRSSFeed(),
    ];
  }

  private function getRSSFeed() {
    $html = "";
    $url = "https://dev.acquia.com/rss.xml";
    $xml = simplexml_load_file($url);
    for($i = 0; $i < 10; $i++){
      $title = $xml->channel->item[$i]->title;
      $link = $xml->channel->item[$i]->link;
      $description = $xml->channel->item[$i]->description;
      $pubDate = $xml->channel->item[$i]->pubDate;

      $html .= "<a href='$link'><h3>$title</h3></a>";
      $html .= "$description";
      $html .= "<br />$pubDate<hr />";
    }
    return $html;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['rss_block_settings'] = $form_state->getValue('rss_block_settings');
  }
}